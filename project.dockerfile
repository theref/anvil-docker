FROM python:3

COPY requirements.txt ./
COPY project/ ./project
RUN pip install -r requirements.txt
